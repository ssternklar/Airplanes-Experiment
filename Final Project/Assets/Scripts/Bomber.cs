﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlaneEngine))]
public class Bomber : AutonomousPlane {

    Vector3 target = new Vector3(10000, 2200, 10000);

	// Use this for initialization
	protected override void Start ()
    {
        engine = GetComponent<PlaneEngine>();
        GameManager.sharedManager.bomber = this;
	}

    protected override void Update()
    {
        GetToPoint(Seek(target));
        if(transform.position.x > target.x && transform.position.z > target.z)
        {
            GameManager.sharedManager.state = 1;
        }
    }

    protected void GetToPoint(Vector3 point)
    {
        Vector3 localVecToPoint = transform.TransformDirection(Seek(point));
        if(Mathf.Abs(transform.position.y - point.y) < 1)
        {
            float angle = (Mathf.Atan2(localVecToPoint.y, localVecToPoint.z) * Mathf.Rad2Deg);
            engine.Pitch(angle);
        }
        else
        {
            float angle = Mathf.Atan2(localVecToPoint.z, localVecToPoint.x);
            engine.Yaw(angle);
        }
    }

    void OnDestroy()
    {
        if(GameManager.sharedManager != null && !GameManager.sharedManager.gameDecided)
        {
            GameManager.sharedManager.state = 2;
        }
    }


}
