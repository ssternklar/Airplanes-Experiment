﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AutonomousPlane))]
public class SetPlayerAsEnemy : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<AutonomousPlane>().opposition = GameObject.Find("F16").GetComponent<PlaneEngine>();
	}
	

	void OnDestroy () {
	    if(GameManager.sharedManager)
        {
            GameManager.sharedManager.state = 1;
        }
	}
}
