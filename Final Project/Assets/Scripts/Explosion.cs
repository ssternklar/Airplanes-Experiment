﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {
    float timer = 0;
    public float maxTimer = 2;

	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if(timer > maxTimer)
        {
            Destroy(gameObject);
        }
	}
}
