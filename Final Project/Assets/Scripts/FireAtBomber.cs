﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AutonomousPlane))]
public class FireAtBomber : MonoBehaviour {

    AutonomousPlane plane;
    void Start()
    {
        plane = GetComponent<AutonomousPlane>();
    }

	// Update is called once per frame
	void Update ()
    {
        if (!plane.opposition && GameManager.sharedManager.bomber && Vector3.Distance(transform.position, GameManager.sharedManager.bomber.transform.position) < 1000 && Vector3.Angle(transform.forward, GameManager.sharedManager.bomber.transform.position - transform.position) < plane.fireAngle)
        {
            float maxAngle = Mathf.Tan(Mathf.Deg2Rad * plane.fireAngle);
            GameManager.sharedManager.GenerateBullet(transform.position + transform.forward * 20, Quaternion.LookRotation(transform.TransformDirection(new Vector3(Random.Range(-maxAngle, maxAngle), Random.Range(-maxAngle, maxAngle), 1))));
        }
	}
}
