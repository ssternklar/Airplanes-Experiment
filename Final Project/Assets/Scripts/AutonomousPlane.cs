﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(PlaneEngine))]
public class AutonomousPlane : MonoBehaviour {

    public PlaneEngine engine;
	public float minRollTolerance = 1;
	public float maxRollTolerance = 45;
	public PlaneEngine opposition;
    public float dogfightTimer;
    public bool debug = false;
    public float fireAngle = 2;
    public float health = 10;

    public float sepWeight = 1;
    public float aliWeight = 1;
    public float cohWeight = 1;
    // Use this for initialization
	protected virtual void Start () {
        engine = GetComponent<PlaneEngine>();
        GameManager.sharedManager.planes.Add(this);
	}

	protected virtual void Update()
	{
        Vector3 dir = Separate(GameManager.sharedManager.planes, GameManager.sharedManager.flockDistance) * sepWeight + TerrainContainment(200) * 10000;
        if(opposition)
        {
            dir += Dogfight(opposition, 1, 6f, 200f);
        }
        else if(GameManager.sharedManager.bomber)
        {
            GameManager.sharedManager.FindFight(this);
            dir += FlockToGameObject(GameManager.sharedManager.bomber.gameObject);
            
        }
        else
        { 
            GameObject b = GameObject.FindWithTag("Bomber");
            if(b)
            {
                GameManager.sharedManager.FindFight(this);
                dir += FlockToGameObject(b);
            }
        }
        GetToDirection(dir);
	}

    protected void GetToDirection(Vector3 direction)
    {
        Vector3 transformedVec = transform.InverseTransformDirection(direction);
        float rollAngle = (Mathf.Atan2(transformedVec.x, transformedVec.y) * Mathf.Rad2Deg);
        float pitchAngle = (Mathf.Atan2(transformedVec.y, transformedVec.z) * Mathf.Rad2Deg);

        if (Mathf.Abs(rollAngle) <= maxRollTolerance)
        {
            engine.Pitch(pitchAngle);

        }
		if(Mathf.Abs(rollAngle) > minRollTolerance)
		{
        	engine.Roll(rollAngle);
		}
    }

    #region Find Direction

    public virtual Vector3 Seek(Vector3 target)
    {
        return (target - transform.position).normalized * engine.maxSpeed;
    }

    public virtual Vector3 Flee(Vector3 target)
    {
        return -Seek(target);
    }

    public virtual Vector3 Pursue(Vector3 target, Vector3 vel, float ticksAhead)
    {
        return Seek(target + (vel * ticksAhead));
    }

    public virtual Vector3 Evade(Vector3 target, Vector3 vel, float ticksAhead)
    {
        return Flee(target + (vel * ticksAhead));
    }

    public virtual Vector3 Dogfight(PlaneEngine enemyEngine, float offensiveScale, float defensiveWaitTime, float defensiveSeparation)
    {
        Vector3 dir = Vector3.zero;
        Vector3 vecToEnemy = enemyEngine.transform.position - transform.position;
        bool offense = false;
        if (Vector3.Dot(transform.forward, vecToEnemy) > 0 || Vector3.Dot(enemyEngine.transform.forward, vecToEnemy) > 0)
        {
            offense = true;
        }
        
        if(offense)
        {
            //Offensive combat is just pursuit. the question is do we want to pursue behind or ahead of the target.
            //To find that, we take 1-(our speed / their speed) so that we know how far ahead we need to seek
            //We then multiply this by a scale to increase or decrease the amount of seek force.
            float predictionAmount = engine.velocity.magnitude > 0 ? (1-(enemyEngine.velocity.magnitude / engine.velocity.magnitude)) * offensiveScale : 0;
            //Pursue the enemy using the prediction amount to figure out how far ahead to go.
            dir = Pursue(enemyEngine.transform.position, enemyEngine.velocity, predictionAmount);
            dogfightTimer = 0;

            //Scale our throttle speed based on the engine speed.
			if (vecToEnemy.magnitude < engine.velocity.magnitude && engine.velocity.magnitude > 0 && Vector3.Dot(enemyEngine.transform.forward, vecToEnemy) > 0)
            {
                engine.throttle = Mathf.Clamp01(vecToEnemy.magnitude / engine.velocity.magnitude);
            }
            else
            {
                engine.throttle = 1;
            }

            if(Vector3.Angle(transform.forward, vecToEnemy) < fireAngle)
            {
                float maxAngle = Mathf.Tan(Mathf.Deg2Rad * fireAngle);
                GameManager.sharedManager.GenerateBullet(transform.position + transform.forward * 25, Quaternion.LookRotation(transform.TransformDirection(new Vector3(Random.Range(-maxAngle, maxAngle), Random.Range(-maxAngle, maxAngle), 1))));
            }
        }
        else
        {
            if (dogfightTimer < defensiveWaitTime || Vector3.Distance(transform.position, enemyEngine.transform.position) > defensiveSeparation)
            {
                engine.throttle = .8f;
                dir = Flee(enemyEngine.transform.position);
                dogfightTimer += Time.deltaTime;
            }
            else
            {
                engine.throttle = .2f;
                dir = Flee(new Vector3(transform.position.x, 0, transform.position.z));
            }
        }
        
        return dir;
    }

    public virtual Vector3 Arrive(Vector3 position, float distance)
    {
        Vector3 vecToPoint = position - transform.position;
        if (engine.velocity.magnitude > 0)
        {
            engine.throttle = Mathf.Clamp01(vecToPoint.magnitude / engine.velocity.magnitude);
        }
        else
        {
            engine.throttle = 1;
        }
        return vecToPoint;
    }

    public virtual Vector3 Separate(List<AutonomousPlane> agents, float separation)
    {
        int count = 0;
        Vector3 sum = new Vector3();
        for (int i = 0; i < agents.Count; i++)
        {
            AutonomousPlane agent = agents[i];
            Vector3 dir = (transform.position - agent.transform.position);
            float dist = dir.magnitude;
            if (dist > 0 && dist < separation)
            {
                sum += dir.normalized / dist;
                count++;
            }
        }
        if (count > 0)
        {
            sum = (sum.normalized * engine.maxSpeed);
        }
        return sum;
    }

    public virtual Vector3 Alignment(List<AutonomousPlane> agents, float separation)
    {
        int count = 0;
        Vector3 sum = new Vector3();
        for (int i = 0; i < agents.Count; i++)
        {
            AutonomousPlane agent = agents[i];
            if (Vector3.Distance(agent.transform.position, transform.position) < separation)
            {
                sum += agent.transform.forward;
                count++;
            }
        }
        if (count > 0)
        {
            sum = sum.normalized * engine.maxSpeed;
        }

        return sum;
    }

    public virtual Vector3 Cohesion(List<AutonomousPlane> agents, float separation)
    {
        int count = 0;
        Vector3 centroid = new Vector3();
        for (int i = 0; i < agents.Count; i++)
        {
            AutonomousPlane agent = agents[i];
            if (Vector3.Distance(agent.transform.position, transform.position) < separation)
            {
                centroid += agent.transform.position;
                count++;
            }
        }
        if (count > 0)
        {
            Vector3 cohesion = Seek(centroid / count);
            cohesion.y = 0;
            return cohesion.normalized * engine.maxSpeed;
        }

        return Vector3.zero;
    }

    public virtual Vector3 FlockToGameObject(GameObject gO)
    {
        Vector3 ret = Vector3.zero;
        ret += Arrive(gO.transform.position, 500);
        ret += Alignment(GameManager.sharedManager.planes, GameManager.sharedManager.flockDistance) * aliWeight;
        ret += Cohesion(GameManager.sharedManager.planes, GameManager.sharedManager.flockDistance) * cohWeight;
        return ret;
    }

	public virtual Vector3 TerrainContainment(float safeHeight)
	{
		if(transform.position.y < safeHeight + Terrain.activeTerrain.SampleHeight(transform.position))
		{
			return Vector3.up * engine.maxSpeed;
		}
		return Vector3.zero;
	}

    #endregion

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if(hit.transform.tag == "Bullet")
        {
            health--;
            if(health < 1)
            {
                GameManager.sharedManager.GenerateExplosion(transform.position);
                Destroy(gameObject);
            }
        }
    }

    void OnDestroy()
    {
        if(GameManager.sharedManager)
        {
            GameManager.sharedManager.planes.Remove(this);
        }
    }
}