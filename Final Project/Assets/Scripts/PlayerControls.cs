﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlaneEngine))]
public class PlayerControls : MonoBehaviour {

    PlaneEngine engine;
    public float health = 5;

	// Use this for initialization
	void Start () {
        engine = GetComponent<PlaneEngine>();
	}
	
	// Update is called once per frame
	void Update () {

        float horiz = Input.GetAxisRaw("Horizontal");
        if (horiz > 0.1f)
        {
            engine.Roll(1000);
        }
        else if(horiz < -0.1f)
        {
            engine.Roll(-1000);
        }

        float vert = Input.GetAxisRaw("Vertical");
        if (vert > 0.1f)
        {
            engine.Pitch(-1000);
        }
        else if (vert < -0.1f)
        {
            engine.Pitch(1000);
        }

        if (Input.GetKey(KeyCode.RightShift))
        {
            engine.throttle += .015f;
            engine.throttle = Mathf.Clamp01(engine.throttle);
        }
        else if (Input.GetKey(KeyCode.LeftShift))
        {
            engine.throttle -= .015f;
            engine.throttle = Mathf.Clamp01(engine.throttle);
        }
        else if(Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            float maxAngle = Mathf.Tan(Mathf.Deg2Rad * 3);
            GameManager.sharedManager.GenerateBullet(transform.position + transform.forward * 25, Quaternion.LookRotation(transform.TransformDirection(new Vector3(Random.Range(-maxAngle, maxAngle), Random.Range(-maxAngle, maxAngle), 1))));
        }
	}

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.transform.tag == "Bullet")
        {
            health--;
            if (health < 1)
            {
                GameManager.sharedManager.GenerateExplosion(transform.position);
                Destroy(gameObject);
            }
        }
    }
    
    void OnDestroy()
    {
        if(GameManager.sharedManager)
        {
            GameManager.sharedManager.state = 2;
        }
    }
}
