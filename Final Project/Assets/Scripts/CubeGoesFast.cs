﻿using UnityEngine;
using System.Collections;

public class CubeGoesFast : MonoBehaviour {

    float speed = 3;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if(transform.position.z > 12000)
        {
            speed = -speed;
            transform.position = new Vector3(transform.position.x, transform.position.y, 11999);
        }
        else if (transform.position.z < 500)
        {
            speed = -speed;
            transform.position = new Vector3(transform.position.x, transform.position.y, 501);
        }
        transform.Translate(0, 0, speed);
	}
}
