﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CameraShifter : MonoBehaviour {

	public List<GameObject> cameras;
	int currentIndex = 0;

    void Start()
    {
        cameras = new List<GameObject>(GameObject.FindGameObjectsWithTag("MainCamera"));
        foreach(GameObject g in cameras)
        {
            g.SetActive(false);
        }
        if (cameras.Find(x => x.name == "PlayerCamera"))
        {
            currentIndex = cameras.FindIndex(x => x.name == "PlayerCamera");
            cameras[currentIndex].SetActive(true);
        }
        else
        {
            currentIndex = 0;
            cameras[0].SetActive(true);
        }
    }
	
	// Update is called once per frame
	void Update () {

        while(!cameras[currentIndex])
        {
            cameras.RemoveAt(currentIndex);
            if(currentIndex >= cameras.Count)
            {
                currentIndex--;
            }
        }
        cameras[currentIndex].SetActive(true);

		if(Input.GetKeyDown(KeyCode.LeftBracket))
        {
            SwapCamera(currentIndex - 1);
        }
        else if(Input.GetKeyDown(KeyCode.RightBracket))
        {
            SwapCamera(currentIndex + 1);
        }
	}

	public void SwapCamera(int index)
    {
        if (cameras[currentIndex])
            cameras[currentIndex].SetActive(false);
        index = ConstrainIndex(index);
        if (cameras[index])
            cameras[index].SetActive(true);
        currentIndex = index;
	}
    
    int ConstrainIndex(int index)
    {
        if (index >= cameras.Count)
            index = 0;
        else if (index < 0)
            index = cameras.Count - 1;
        return index;
    }
}
