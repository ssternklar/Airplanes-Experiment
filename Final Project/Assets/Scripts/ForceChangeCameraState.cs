﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CameraShifter))]
public class ForceChangeCameraState : MonoBehaviour {
    bool hasForcedChange = false;
	
	// Update is called once per frame
	void Update () {
        if (!hasForcedChange)
        {
            CameraShifter shifter = GetComponent<CameraShifter>();
            shifter.SwapCamera(shifter.cameras.FindIndex(x => x.transform.parent.GetComponent<PlayerControls>() != null));
            hasForcedChange = true;
        }
	}
}
