﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public static GameManager sharedManager;
    public GameObject bulletPrefab;
    public GameObject explosionPrefab;
    public List<AutonomousPlane> planes;
    public Bomber bomber;
    public float flockDistance = 50;

    public int state = 0;
    public bool gameDecided = false;
    void Awake()
    {
        sharedManager = this;
        planes = new List<AutonomousPlane>();
    }

    public void GenerateBullet(Vector3 position, Quaternion rotation)
    {
        Instantiate(bulletPrefab, position, rotation);
    }

    public void GenerateExplosion(Vector3 position)
    {
        Instantiate(explosionPrefab, position, Quaternion.identity);
    }

    public void FindFight(AutonomousPlane plane)
    {
        for(int i = 0; i < planes.Count; i++)
        {
            if(!planes[i].CompareTag(plane.tag) && !planes[i].opposition && Vector3.Distance(plane.transform.position, planes[i].transform.position) < 3000)
            {
                planes[i].opposition = plane.engine;
                plane.opposition = planes[i].engine;
            }
        }
    }
    public void Update()
    {
        flockDistance = 5.2f * planes.Count;
        if(state == 1)
        {
            Application.LoadLevel("Bomber Success");
            gameDecided = true;
        }
        if(state == 2)
        {
            Application.LoadLevel("Bomber Failure");
            gameDecided = true;
        }
    }
}
