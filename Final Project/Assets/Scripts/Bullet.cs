﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour {

	// Use this for initialization

    public Vector3 velocity;
    public float maxSpeed = 500;
    public float maxTimeAlive = 0;
    float timer = 0;
	void Start () {
        velocity = transform.forward * maxSpeed;
	}

	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if(timer > maxTimeAlive)
        {
            Destroy(gameObject);
        }
        GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + (velocity * Time.deltaTime));
	}
}
