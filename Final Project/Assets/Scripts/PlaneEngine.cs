﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class PlaneEngine : MonoBehaviour {
    
    CharacterController controller;

    public float gravity = 10f;
	public float maxYawDegrees = 15f;
    public float maxRollDegrees = 45f;
    public float maxNegativePitchDegrees = 30f;
    public float maxPositivePitchDegrees = 60f;
    public float throttle = .7f;
    public float maxSpeed = 100f;
    public Vector3 velocity;
    public float throttleChangeAmount = .015f;
    // Use this for initialization
	void Start () {
        controller = GetComponent<CharacterController>();
        maxNegativePitchDegrees = -maxNegativePitchDegrees;
        velocity = transform.forward * (maxSpeed * throttle);
	}
	
	// Update is called once per frame
	void Update () {
        velocity = transform.TransformDirection(0, 0, maxSpeed * throttle);
        velocity.y -= gravity * (1f-throttle);
        controller.Move(velocity * Time.deltaTime);
    }

    public void Yaw(float amount)
    {
        amount = Mathf.Clamp(amount, -maxYawDegrees * Time.deltaTime, maxYawDegrees * Time.deltaTime);
        transform.Rotate(0, -amount, 0);
    }

    public void Roll(float amount)
    {
        amount = Mathf.Clamp(amount, -maxRollDegrees * Time.deltaTime, maxRollDegrees * Time.deltaTime);
        transform.Rotate(0, 0, -amount);
    }

    public void Pitch(float amount)
    {
        amount = Mathf.Clamp(amount, maxNegativePitchDegrees * Time.deltaTime, maxPositivePitchDegrees * Time.deltaTime);
        transform.Rotate(-amount, 0, 0);
    }
}
