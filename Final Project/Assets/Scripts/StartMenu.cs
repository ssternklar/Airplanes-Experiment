﻿using UnityEngine;
using System.Collections;

public class StartMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void OnGUI()
    {
        if(GUI.Button(new Rect(Screen.width * .25f - 50, Screen.height / 2f - 50, 100, 100), "Main Scene"))
        {
            Application.LoadLevel("Actual Scene");
        }
        else if (GUI.Button(new Rect(Screen.width * .5f - 75, Screen.height / 2f - 50, 150, 100), "Player Control Mode"))
        {
            Application.LoadLevel("Player Dogfighting");
        }
        else if (GUI.Button(new Rect(Screen.width * .75f - 50, Screen.height / 2f - 50, 100, 100), "WAAAGH!!"))
        {
            Application.LoadLevel("Plane Waaagh!!");
        }
    }
}
